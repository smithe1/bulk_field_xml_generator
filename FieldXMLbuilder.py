#############################################################################
#
# Description: Creates an XML file called "GeneratedXML.xml" formatted in
# such a way that can be added to the object XML and then deployed with ANT
# Author: Eric Smith (Bits In Glass)
# Date: May 2017
#
#############################################################################
import sys

num_args = len(sys.argv)
arg_list = sys.argv

format_labels = False # When true: Will Capitalize() each word in the string
format_names = True # When true: Will append __c to the end of all field API names.

if(num_args > 1):
	for arg in arg_list:
		split_arg = arg.split('=')
		argLabel = split_arg[0].strip()
		argVal = ''

		if(argLabel == 'format_names'):
			if(len(split_arg) > 1):
				argVal = split_arg[1].strip()
				format_names = True if (argVal.capitalize() == 'True' or argVal == '1') else False
				print format_names
			else:
				format_labels = False

		if(argLabel == 'format_labels'):
			if(len(split_arg) > 1):
				argVal = split_arg[1].strip()
				format_labels = True if (argVal.capitalize() == 'True' or argVal == '1') else False
				print format_labels
			else:
				format_labels = False


file = open("FieldInputData.csv", "r")

newFile = open("GeneratedXML.xml", "w")

labelLine = file.readline()
print labelLine
labels = labelLine.strip().split(",")
labelsXML = []
print labels
	
for label in labels:
	labelsXML.append(('<'+label+'>', '</'+label+'>'))

print labelsXML

for line in file:
	fieldString = '<fields>\n'
	index = 0
	values = line.strip().split(",")
	for item in values:
		if(item != ''):
			if((labels[index] == 'fullName') and (format_names == True)):
				updatedValue = item.strip().split()
				item = ''
				for word in updatedValue:
					word = word.capitalize()
					item += word + '_'
				item = item.strip()				
				updatedValue = item.strip() + '_c'
				item = updatedValue
			if((labels[index] == 'label') and (format_labels == True)):
				updatedValue = item.strip().split()
				item = ''
				for word in updatedValue:
					word = word.capitalize()
					item += word + ' '
				item = item.strip()

			subItemXML = '\t' + labelsXML[index][0] + item + labelsXML[index][1] + '\n';
			fieldString += subItemXML
			if((labels[index] == 'type') and (item.capitalize() == 'Picklist')):
				subItemXML = '\t<picklist>\n\t\t<picklistValues>\n\t\t\t<fullName>Placeholder</fullName>\n\t\t\t<default>True</default>\n\t\t</picklistValues>\n\t</picklist>\n';
				fieldString += subItemXML

		index += 1
	fieldString += '</fields>\n'

	newFile.write(fieldString)

