The FieldXMLbuilder is a tool designed to format a CSV file into XML in the 
format that can be deployed to Salesforce using a tool like ANT.

The purpose is so that you create fields on a salesforce object in bulk without having 
to go through the whole wizard every time.

Run With:
	python FieldXMLBuilder.py [args]

Input:
	- A .csv file named "FieldInputData.csv"
	- There is an example file in the same repository.
	- You can add any columns you would like to this file and the column header will be used as an XML tag. 
	Any optional tags can be applied and some tags will be required for certain Metadata field types:
		All - Will need 'type', 'fullName', and 'label' as per the template
		Text - requires a 'length' tag and value
		Number - requires both a 'precision' (number of digits) and 'scale' number of decimal places
		Picklist - required tags added by script with a 'Placeholder' default value. You will
				have to add picklist values manually in Salesforce.
		Date - No additional requirements
		DateTime - No additional requirements
		
		Any other requirements can be found using the Metadata API Developer Guide:
			https://developer.salesforce.com/docs/atlas.en-us.api_meta.meta/api_meta/meta_field_types.htm

Output:
	- A .xml file called "GeneratedXML.xml"
	- You will need to paste this entire file into an object XML file extracted from your org. Paste the
	first field underneath any existing fields in the XML or if there are not any paste it under the
		'<enableStreamingApi>true</enableStreamingApi>'
	tag. The tags are just by default sorted alphabetically so it doesn't actually matter where in the 
	file they exist.

Command Line Arguments:

	format_labels
		Default = False
		When True: Capitalizes the first letter of each word in the label and makes all other letters 
		lowercase.

	format_names
		Default = True
		When True: Replaces spaces with underscores and appends '__c' to the end as Salesforce would.
		The purpose is so that you can just copy over your labels into the fullName column and have
		them formatted as Salesforce likes them

	eg:
		python FieldXMLBuilder.py format_labels=true format_names=true

		OR

		python FieldXMLBuilder.py format_labels=1 format_names=0

Deployment:

	Deploy the object as you normally would using the ANT tool.